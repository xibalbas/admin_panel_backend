from django.contrib.auth.models import AbstractUser, AbstractBaseUser
from django.db import models
from django.utils.translation import gettext as _

# class CustomUser(AbstractUser):
#     pass

class CustomUser(AbstractUser):
    ANNUAL_INCOME_CHOICES = (
        ('1', '$25000-$50000'),
        ('2', '$50000-$100000'),
        ('3', '$100000-$200000'),
        ('4', '$200000-$1000000'),
        ('5', '$1000000+'),

    )
    LIQUID_NET = (
        ('1', '$25000-$50000'),
        ('2', '$50000-$100000'),
        ('3', '$100000-$200000'),
        ('4', '$200000-$1000000'),
        ('5', '$1000000+'),

    )

    is_email_verified = models.BooleanField(
        verbose_name=_('Email Verified'),
        default=False
    )

    is_phone_verified = models.BooleanField(
        verbose_name=_('Phone Verified'),
        default=False
    )

    middle_name = models.CharField(
        verbose_name=_('First Name'),
        max_length=150,
        null=True,
        blank=True
    )
    address_line_1 = models.CharField(
        verbose_name=_('Address Line 1'),
        max_length=300,
        null=True,
        blank=True
    )
    address_line_2 = models.CharField(
        verbose_name=_('Address Line 2'),
        max_length=300,
        null=True,
        blank=True
    )
    city = models.CharField(
        verbose_name=_('City'),
        max_length=50,
        null=True,
        blank=True
    )
    state = models.CharField(
        verbose_name=_('State'),
        max_length=50,
        null=True,
        blank=True
    )
    country = models.CharField(
        verbose_name=_('Country'),
        max_length=50,
        null=True,
        blank=True
    )
    phone = models.CharField(
        unique=True,
        verbose_name=_('Phone'),
        max_length=50,
        null=True,
        blank=True
    )

    postal_code = models.CharField(
        unique=True,
        verbose_name=_('Postal Code'),
        max_length=50,
        null=True,
        blank=True
    )

    date_birth = models.DateField(
        verbose_name=_('Date Birth'),
        null=True,
        blank=True
    )
    annual_income = models.CharField(
        verbose_name=_('Annual Income'),
        max_length=1,
        choices=ANNUAL_INCOME_CHOICES,
        null=True,
        blank=True
    )
    liquid_net = models.CharField(
        verbose_name=_('Liquid Net'),
        max_length=1,
        choices=LIQUID_NET,
        null=True,
        blank=True
    )
    question1 = models.BooleanField(
        verbose_name=_('Question1'),
        null=True,
        blank=True
    )
    question2 = models.BooleanField(
        verbose_name=_('Question2'),
        null=True,
        blank=True
    )
    question3 = models.BooleanField(
        verbose_name=_('Question3'),
        null=True,
        blank=True
    )

    REQUIRED_FIELDS = ['password', 'email', 'first_name', 'last_name', 'country', 'question1', 'question2',
                       'question3']

    def __str__(self):
        return self.username
