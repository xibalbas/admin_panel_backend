# Create your views here.
from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken

from admin_panel_backend import settings
from user.models import CustomUser
from jwt import decode as jwt_decode
from jwt.exceptions import DecodeError
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from .serializers import CustomUserSerializer
from historical_data.tasks import spot_daily_update


class SignUpUser(APIView):
    @staticmethod
    def post(request):
        serializer = CustomUserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            data = {
                "detail": "success"
            }
            return Response(data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TokenValidation(APIView):

    @staticmethod
    def get(request):
        token = request.GET.get('token', "123456")
        try:
            # try to validate entered token , if token is valid we grab user_id from decoding token
            decoded_data = jwt_decode(token, settings.SECRET_KEY, algorithms=[settings.SIMPLE_JWT["ALGORITHM"]])
            return Response({"message": "valid"}, status=status.HTTP_200_OK)

        except(InvalidToken, TokenError, DecodeError) as e:
            # if token isn't valid we return None as a user
            return Response({"message": "not valid"}, status=status.HTTP_400_BAD_REQUEST)


class Token(APIView):
    """ get jwt access token for all type of users ( managers or users ) """
    permission_classes = ()

    @staticmethod
    def post(request):
        username = request.data["username"]
        password = request.data["password"]

        # check user is exists
        try:
            user = CustomUser.objects.get(username=username)
        except CustomUser.DoesNotExist:
            response = {
                'detail': 'error',
                'message': 'invalid username or password'
            }

            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        # check if password is valid
        if user.check_password(password):

            # get access token for user from JWT pre-existing method
            user_token = RefreshToken.for_user(user)

            user_groups = [i.name for i in user.groups.all()]

            # get user role
            user_role = ''
            if 'admin' in user_groups:
                user_role = 'admin'
            elif 'manager' in user_groups:
                user_role = 'manager'
            else:
                user_role = 'user'

            response = {
                'username': username,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                "is_email_verified": user.is_email_verified,
                "is_phone_verified": user.is_phone_verified,
                "middle_name": user.middle_name,
                "address_line_1": user.address_line_1,
                "address_line_2": user.address_line_2,
                "city": user.city,
                "state": user.state,
                "country": user.country,
                "phone": user.phone,
                "postal_code": user.postal_code,
                "date_birth": user.date_birth,
                "annual_income": user.annual_income,
                "liquid_net": user.liquid_net,
                "question1": user.question1,
                "question2": user.question2,
                "question3": user.question3,
                'role': user_role,
                'token': str(user_token.access_token),
            }

            return Response(response, status=status.HTTP_200_OK)
        else:
            response = {
                'detail': 'error',
                'message': 'invalid username or password'
            }

            return Response(response, status=status.HTTP_400_BAD_REQUEST)


class UserNumberAPi(APIView):
    permission_classes = ([])

    @staticmethod
    def get(request):
        user = CustomUser.objects.all().count()

        response = {
            'detail': 'success',
            'user_number': user
        }

        return Response(response, status=status.HTTP_200_OK)


class UserProfileApi(APIView):
    permission_classes = ([])

    @staticmethod
    def get(request, user_id):
        user = CustomUser.objects.get(pk=user_id)
        serializer = CustomUserSerializer(user, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)
