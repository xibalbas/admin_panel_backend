from django.urls import path
from .views import UserProfileApi, UserNumberAPi, SignUpUser


urlpatterns = [
    path('user_number/', UserNumberAPi.as_view()),
    path('user_profile/<int:user_id>/', UserProfileApi.as_view()),
    path('sign_up/', SignUpUser.as_view())
]
