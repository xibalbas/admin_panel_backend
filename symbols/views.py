from datetime import datetime, timedelta, date

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from common.symbols import spot_symbols
from historical_data.crypto_daily_model import DailyCrypto


class SymbolsListApi(APIView):
    """ list all symbols """

    @staticmethod
    def get(request):
        symbols = DailyCrypto.objects.filter().distinct("symbol")
        symbols = [i.symbol for i in symbols]

        response = {
            "detail": "success",
            "data": symbols
        }

        return Response(response, status=status.HTTP_200_OK)


class SymbolHistoryApi(APIView):
    permission_classes = ([])

    @staticmethod
    def get(request):
        symbol = request.GET.get("symbol")
        symbols = DailyCrypto.objects.filter(symbol=symbol).order_by('date')
        response = {
            't': [],
            'o': [],
            'h': [],
            'l': [],
            'c': [],
            'v': [],
            's': 'ok'
        }

        for i in symbols:
            response['t'].append(int(datetime.strptime(
                str(i.date), '%Y-%m-%d').strftime("%s")))
            response['o'].append(i.open)
            response['h'].append(i.high)
            response['l'].append(i.low)
            response['c'].append(i.close)
            response['v'].append(i.volume)

        return Response(response, status=status.HTTP_200_OK)
