from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from .tasks import spot_daily_update


# Create your views here.
class TestTask(APIView):
    @staticmethod
    def get(request):
        spot_daily_update()
        return Response({"detail": "succeed"})
